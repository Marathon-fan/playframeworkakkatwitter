package controllers

import javax.inject._
import play.api.mvc._
import play.api.libs.oauth._
//import play.api.libs.oauth.ConsumerKey
//import play.libs.oauth.OAuth.RequestToken
//import play.libs.oauth.OAuth
//import play.libs.oauth.OAuth.ServiceInfo
//     play.libs.oauth.OAuth.ConsumerKey

import play.api.Play.current
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits._

// use silhouette //https://www.silhouette.rocks/
// also, change to oauth2 to simplify the program
// use linkedin API
// we all like simplicity, don't we?

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
    * Create an Action to render an HTML page with a welcome message.
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/`.
    */
  val KEY = ConsumerKey("xxxxx", "xxxxx")

  //def myOauth = new OAuth()
  val oauth = new Oauth( new ServiceInfo(
    "https://api.twitter.com/oauth/request_token",
    "https://api.twitter.com/oauth/access_token",
    "https://api.twitter.com/oauth/authorize", KEY),
    true)


  def index = Action {

    oncePerSecond(() =>
      println("Time flies like an arrow...")
    )

    Ok(views.html.index("Your new application is ready. Play 2,6.1 "))


  }

  def tweets = Action {
    Ok(views.html.index("Your new application is ready. Play 2,6.1 "))
  }

  def oncePerSecond(callback: () => Unit){
    while (true){ callback(); Thread sleep 1000
    }
  }

}

  /*

  def tweets = Action.async {

    val credentials: Option[(ConsumerKey, RequestToken)] = for {
      apiKey <- Play.configuration.getString("twitter.apiKey")
      apiSecret <- Play.configuration.getString("twitter.apiSecret")
      token <- Play.configuration.getString("twitter.token")
      tokenSecret <- Play.configuration.getString("twitter.tokenSecret")
    } yield {
      ConsumerKey(apiKey, apiSecret);
      RequestToken(token, tokenSecret)
    }

    credentials.map { case (ConsumerKey, RequestToken) =>
      Future.successful{
        Ok
      }
    } getOrElse {
      Future.successful {
        InternalServerError("Twitter credentials missing")
      }
    }
  }

}

//--
val KEY = ConsumerKey("xxxxx", "xxxxx")

val oauth = OAuth(ServiceInfo(
"https://api.twitter.com/oauth/request_token",
"https://api.twitter.com/oauth/access_token",
"https://api.twitter.com/oauth/authorize", KEY),
true)

def sessionTokenPair(implicit request: RequestHeader): Option[RequestToken] = {
  for {
  token <- request.session.get("token")
  secret <- request.session.get("secret")
} yield {
  RequestToken(token, secret)
}
}

*/